# LIS4381 - Mobile Web Application Development

## Joseph Fernandez

### Project 1 Requirements:

1. Screenshot of running application's first user interface
2. Screenshot of running application's second user interface
3. Chapters 5 and 6 Questions answered

#### Project Screenshots:

*Screenshot of application's first user interface running*:

![First User Interface](img/intp1.png)

*Screenshot of application's second user interface running*:

![Second User Interface](img/intp1_2.png)