# LIS4381 - Mobile Web Application Development

## Joseph Fernandez

### LIS4381 Requirements:

*Course Work Links (Relative link example):*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    
    - Install AMPPS
    - Install JDK
    - Install Android Studio and create My First App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
    - Provide git command descriptions
    - Chapters 1 and 2 Questions answered

2. [A2 README.md](a2/README.md "My A2 README.md file")

    - Screenshot of running application's first user interface
    - Screenshot of running application's second user interface
    - Chapters 3 and 4 Questions answered

2. [A3 README.md](a3/README.md "My A3 README.md file")

    - Screenshot of ERD
    - Screenshot of running application's first user interface
    - Screenshot of running application's second user interface
    - Links to the following files:
        a. a3.mwb
        b. a3.sql
    - Chapters 5 and 6 Questions answered

2. [P1 README.md](p1/README.md) "My P1 README.md file")
    
    - Screenshot of running application's first user interface
    - Screenshot of running application's second user interface
    - Chapters 7 and 8 Questions answered
    
2. [A4 README.md](a4/README.md "My A4 README.md file")

    - Screenshot of LIS4381 Main Page
    - Screenshot of A4 Failed Validation
    - Screenshot of A4 Passed Validation
    - Chapters 9, 10, and 15 Questions answered

2. [A5 README.md](a5/README.md "My A5 README.md file")

    - Screenshot of A4 index.php
    - Screenshot of A4 add_petstore_process.php with error
    - Chapters 11, 12, 19 Questions answered

6. [P2 README.md](p2/README.md "My P2 README.md file")

    - Screenshot of Project 2 Index.php
    - Screenshot of Project 2 edit_petstore.php
    - Screenshot of Project 2 edit_petstore_process.php
    - Screenshot of Homepage with content
    - Screenshot of Project 2 RSS Feed
    - Chapters 13-14 Questions answered
