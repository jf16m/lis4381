# LIS4381 - Mobile Web Application Development

## Joseph Fernandez

### Project 2 Requirements:

1. Screenshot of Project 2 index.php
2. Screenshot of Project 2 edit_petstore.php
3. Screenshot of Project 2 Server-side validation / edit_petstore_process.php
4. Screenshot of Homepage with added content.
5. Screenshot of Project 2 RSS Feed.
6. Chs. 13-14 Questions completed.

#### Project Screenshots:

*Screenshot of Project 2 index.php*:

![Edit-delete functionality](img/p2_edit_delete.png)

*Screenshot of Project 2 edit_petstore.php*:

![Edit petstore page](img/p2_edit.png)

*Screenshot of Project 2 Validation*:

![Server-side validation](img/p2_vali.png)

*Screenshot of Homepage*:

![Homepage screenshot](img/p2_homepage.png)

*Screenshot of Project 2 RSS Feed*:

![RSS Feed](img/p2_rssfeed.png)
