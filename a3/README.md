# LIS4381 - Mobile Web Application Development

## Joseph Fernandez

### Assignment 3 Requirements:

1. Screenshot of ERD
2. Screenshot of running application's first user interface
3. Screenshot of running application's second user interface
4. Links to the following files:
    - a3.mwb
    - a3.sql
5. Chapters 5 and 6 Questions answered

#### Assignment Screenshots:

*Screenshot of A3 ERD*:

![Assignment 3 ERD](img/a3erd.png)

*Screenshot of application's first user interface running*:

![First User Interface](img/intfirst.png)

*Screenshot of application's second user interface running*:

![Second User Interface](img/intsecond.png)

*A3 mwb File link*:

[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")

*A3 sql File link*:

[A3 SQL File](docs/a3.sql "A3 SQL Script")