import java.util.Scanner;

class LargestNumber
{
   public static void main(String args[])
   {
      System.out.println("Program Evaluates largest of two integers.");
      System.out.println("Note: Program does *not* check for non-numeric characters or non-integer values.");
      int x;
      int y;

      System.out.print("Enter first integer: ");
      Scanner in = new Scanner(System.in);
      x = in.nextInt();

      System.out.print("Enter second integer: ");
      y = in.nextInt();

      if ( x > y )
 	System.out.println( x + " is larger than " + y);
      else if ( x < y )
        System.out.println( y + " is larger than " + x);
      else
	System.out.println("Integers are equal");
   }
}
