import java.util.Scanner;
 
class EvenOrOdd
{
   public static void main(String args[])
   {
      System.out.println("Program Evaluates integers as even or odd.");

      int x;
      System.out.println("Enter integer: ");
      Scanner in = new Scanner(System.in);
      x = in.nextInt();
 
      if ( x % 2 == 0 )
         System.out.println( x + " is an even number.");
      else
         System.out.println( x + " is an odd number.");
   }
}
