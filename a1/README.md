# LIS4381 - Mobile Web Application Development

## Joseph Fernandez

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket.
2. Development Installations.
3. Chapter Questions (Chs. 1,2)

#### README.md file should include the following items:

* Screenshot of AMPPS Installation My PHP Installation
* Screenshot of running java Hello
* Screenshot of running Android Studio - My First App
* git commands w/short descriptions
* Bitbucket repo links; a) thi assignment and b) the completed tutorials above (bitbucketstationlocations and myteamquotes)


#### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch 
7. git status - List the files you've changed and those you still need to add or commit

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/jf16m/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/jf16m/myteamquotes/ "My Team Quotes Tutorial")