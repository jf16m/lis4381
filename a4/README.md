# LIS4381 - Mobile Web Application Development

## Joseph Fernandez

### Assignment 4 Requirements:

1. Screenshot of LIS4381 Main Page
2. Screenshot of A4 Failed Validation
3. Screenshot of A4 Passed Validation
4. Chapters 9, 10, and 15 Questions answered

#### Assignment Screenshots:

*Screenshot of LIS4381 Main Page*:

![Main Page](img/mainpage.png)

*Screenshot of A4 Failed Validation*:

![Failed Validation](img/failedvali.png)

*Screenshot of A4 Passed Validation*:

![Passed Validation](img/passvali.png)

*http://localhost/repos/lis4381/index.php*