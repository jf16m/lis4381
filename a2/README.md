# LIS4381 - Mobile Web Application Development

## Joseph Fernandez

### Assignment 2 Requirements:

*Two Parts:*

1. Screenshot of running application's first user interface.
2. Screenshot of running application's second user interface.
3. Chapter 3 and 4 Questions.

#### Assignment Screenshots:

*Screenshot of application's first user interface running*:

![First User Interface](img/firstint.png)

*Screenshot of application's second user interface running*:

![Second User Interface](img/secondint.png)