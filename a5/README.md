# LIS4381 - Mobile Web Application Development

## Joseph Fernandez

### Assignment 5 Requirements:

1. Screenshot of index.php
2. Screenshot of add_petstore_process.php and accompanying error
3. Chapters 11, 12, and 19 Questions completed.

#### Assignment Screenshots:

*Screenshot of index.php*:

![index](img/a4index.png)

*Screenshot of A4 Failed Validation*:

![add_petstore_process](img/a4process.png)

*http://localhost/repos/lis4381/index.php*